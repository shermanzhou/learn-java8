package com.shermanzhou.learn.learnjava8.lambda;

/**
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/9/29 下午5:47
 */
public class Exercise<T,R> {


    public String getValue(String str, StringUpper stringUpper) {
        return stringUpper.getValue(str);
    }

    public Long calc(long l1, long l2, FuntionalInterfaceWithTwoGenericType<Long,Long> FI) {
        return FI.calc(l1, l2);
    }
}
