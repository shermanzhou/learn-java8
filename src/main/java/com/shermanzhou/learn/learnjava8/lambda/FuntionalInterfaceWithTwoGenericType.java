package com.shermanzhou.learn.learnjava8.lambda;

/**
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/9/29 下午5:56
 */
@FunctionalInterface
public interface FuntionalInterfaceWithTwoGenericType<T,R> {
    R calc(T t1, T t2);
}
