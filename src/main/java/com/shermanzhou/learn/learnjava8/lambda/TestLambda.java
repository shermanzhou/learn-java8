package com.shermanzhou.learn.learnjava8.lambda;

import org.junit.Before;
import org.junit.Test;

import com.shermanzhou.learn.learnjava8.streamapi.model.Employee;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/9/29 下午3:17
 */
public class TestLambda {

    List<Employee> employees = null;

    Exercise exercise = new Exercise();

    @Before
    public void before () {
        employees = Arrays.asList(
                new Employee("bb", 22, 3333D),
                new Employee("cc", 44, 4444D),
                new Employee("dd", 55, 5555D),
                new Employee("aa", 22, 2222D)
        );
    }

    @Test
    public void test1 () {
        Comparator<Integer> comparator = (x, y) -> Integer.compare(x, y);
        int compare = comparator.compare(1, 2);
        System.out.println(compare);

    }

    @Test
    public void test2 () {
        Collections.sort(employees, (x,y) -> {
            if (x.getAge() == y.getAge()) {
                return x.getName().compareTo(y.getName());
            }
            return Integer.compare(x.getAge(), y.getAge());
        });

        employees.forEach(System.out::println);
    }

    @Test
    public void test11() {

    }

    String getValue(String str, Function<String, String> function) {
        return function.apply(str);
    }

    @Test
    public void test33 () {
        String aaaa = getValue("aaaa", (x) -> x.toUpperCase());
        System.out.println(aaaa);

        String value = getValue("12312456", (x) -> x.substring(2, 4));
        System.out.println(value);
    }
    @Test
    public void test3 () {

        String asdkjha = exercise.getValue("asdkjha", (x) -> x.toUpperCase());
        System.out.println(asdkjha);

        String value = exercise.getValue("12312456", (x) -> x.substring(2, 4));
        System.out.println(value);

    }

    <T,R> R calc(T a, T b, BiFunction<T,T,R> function) {
        return function.apply(a, b);
    }

    @Test
    public void test44 () {
        Long sum = calc(123L, 123L, (x, y) -> x + y);
        System.out.println(sum);
        Long product = calc(123L, 123L, (x, y) -> x * y);
        System.out.println(product);
    }
    @Test
    public void test4 () {
        long sum = exercise.calc(123L, 123L, ( x,  y) -> ((Long)x + (Long)y));
        System.out.println(sum);

        long product = exercise.calc(123L, 123L, ( x,  y) -> ((Long)x * (Long)y));
        System.out.println(product);
    }

    @Test
    public void test5 () {
        String a = "hello2";
        final String b = "hello";
        String d = "hello";
        String c = b + 2;
        String e = d + 2;
        System.out.println((a == c));
        System.out.println((a == e));
    }

    @Test
    public void test6 () {
        String a = "hello2";
        String d = "hello2";
        System.out.println((a == d));
    }
}
