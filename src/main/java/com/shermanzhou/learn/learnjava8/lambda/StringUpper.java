package com.shermanzhou.learn.learnjava8.lambda;

/**
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/9/29 下午5:45
 */
public interface StringUpper {
    String getValue(String str);
}
