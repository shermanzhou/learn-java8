package com.shermanzhou.learn.learnjava8.streamapi.model;

/**
 * 交易员类
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/10/3 上午10:36
 */
public class Trader {
    private String name;
    private String city;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Trader(String name, String city) {
        this.name = name;
        this.city = city;
    }

    public Trader() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Trader)) return false;

        Trader trader = (Trader) o;

        if (name != null ? !name.equals(trader.name) : trader.name != null) return false;
        return city != null ? city.equals(trader.city) : trader.city == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Trader{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }


}
