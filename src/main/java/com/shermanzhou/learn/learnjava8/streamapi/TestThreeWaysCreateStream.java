package com.shermanzhou.learn.learnjava8.streamapi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/10/2 下午12:13
 */
public class TestThreeWaysCreateStream {

    private long start;
    private long end;

    @Before
    public void before() {
        start = System.currentTimeMillis();
    }

    @After
    public void after() {
        end = System.currentTimeMillis();
        System.out.println("cost: " + (end - start) + " ms");
    }

    @Test
    public void test1() {
        //1. Stream.of()
        // 独立的多个值
        Stream<String> stream = Stream.of("aa", "bb", "cc");
        stream.map((x) -> x.toUpperCase()).forEach(System.out::println);

        // 数组
        String[] strings = {"aaa", "bbb", "ccc"};
        stream = Stream.of(strings);
        stream.map(String::toUpperCase).forEach(System.out::println);

        //2. Arrays.stream
        String[] strings1 = {"aaaa", "bbbb", "cccc"};
        stream = Arrays.stream(strings1);
        stream.map(String::toUpperCase).forEach(System.out::println);

        //3. Collections
        List<String> list = Arrays.asList("AA", "BB", "CC");
        stream = list.stream();
        stream.map(String::toLowerCase).forEach(System.out::println);

        stream = list.stream();
        Set<String> set = stream.collect(Collectors.toSet());
        set.stream().map((x)->x+'~').forEach(System.out::println);



    }
}
