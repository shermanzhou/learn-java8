package com.shermanzhou.learn.learnjava8.streamapi;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/10/2 下午12:54
 */
public class TestStructureConversion {

    private long start;
    private long end;

    @Before
    public void before() {
        start = System.currentTimeMillis();
    }

    @After
    public void after() {
        end = System.currentTimeMillis();
        System.out.println("cost: " + (end - start) + " ms");
    }

    @Test
    public void test1() {
        List<String> list = new LinkedList<>();
        list.addAll(Arrays.asList("aa", "bb", "cc", "aa", "bb", "cc", "aa", "bb", "cc"));

        Set<String> arrayList = list.stream()
                .map((x) -> x + "~~~")
                .collect(Collectors.toCollection(HashSet::new));
        System.out.println(arrayList);
    }

    @Test
    public void test2() {
        List<String> list = new LinkedList<>();
        list.addAll(Arrays.asList("aa", "bb", "cc", "aa", "bb", "cc", "aa", "bb", "cc"));

        String all = list.stream().collect(Collectors.joining());
        System.out.println(all);

        Stack<String> collect = list.stream().collect(Collectors.toCollection(Stack::new));
        System.out.println(collect);
    }
}
