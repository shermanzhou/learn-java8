package com.shermanzhou.learn.learnjava8.streamapi.exercise;

import com.shermanzhou.learn.learnjava8.streamapi.model.Employee;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/10/3 上午10:21
 */
public class TestStreamAPI2 {

    List<Employee> employees = Arrays.asList(
            new Employee("aa", 11, 1111, Employee.Status.FREE),
            new Employee("bb", 11, 2222, Employee.Status.BUSY),
            new Employee("cc", 33, 3333, Employee.Status.FREE),
            new Employee("dd", 33, 4444, Employee.Status.VOCATION),
            new Employee("ee", 44, 3333, Employee.Status.FREE)
    );

    /**
     * 1. 给定一个数字列表，如何返回一个由每个数的平方构成的列表呢？
     *    给定【1，2，3，4，5】，应该返回【1，4，9，16，25】
     */
    @Test
    public void test1() {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);
        List<Integer> squares = integers.stream()
                .map((x) -> x * x)
                .collect(Collectors.toList());
        System.out.println(squares);
    }

    /**
     * 2. 怎么用 map 和 reduce 数一数流中有多少个 Employee 呢？
     */
    @Test
    public void test2() {
        Integer count = employees.stream()
                .map((x) -> 1)
                .reduce(0, Integer::sum);
        System.out.println(count);
    }
}
