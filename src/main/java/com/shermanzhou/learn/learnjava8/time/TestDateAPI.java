package com.shermanzhou.learn.learnjava8.time;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author zhouchaoqun
 * @email zhouchaoqun@quancheng-ec.com
 * @created 2017/10/10 上午9:44
 */
public class TestDateAPI {

    @Test
    public void test1() {
        String format = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        System.out.println(format);
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME.withZone(ZoneId.of("Asia/Shanghai"));
        ZoneId zone = formatter.getZone();
        System.out.println(zone);

        format = ZonedDateTime.now(zone).format(formatter);
        System.out.println(format);

    }
}
